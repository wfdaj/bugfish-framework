# Configuring Apache with mod_expires for Content Caching Control

This guide explains how to configure Apache with `mod_expires` directives to control content caching. The `mod_expires` module allows you to set expiration headers for different types of files, enabling you to control how long client browsers should cache content.

## Prerequisites

Before you proceed, ensure the following prerequisites are met:

- Apache web server is installed and running.
- The `mod_expires` module is enabled.

## Procedure

Follow these steps to configure Apache with `mod_expires` for content caching control:

1. **Enable the `expires` Module:**

   Ensure that the `expires` module is enabled. You can enable it using the following command:

   ```bash
   sudo a2enmod expires
   ```

2. **Create or Edit the VirtualHost Configuration File:**

   Locate the VirtualHost configuration file for your website or application. This file is typically found in the `/etc/apache2/sites-available/` directory and has a `.conf` extension. You can create a new one if it doesn't exist.

3. **Edit the VirtualHost Configuration:**

   Inside the VirtualHost configuration file, add the `mod_expires` directives to specify the expiration headers for different file types:

   ```apache
   <VirtualHost *:80>
       ServerName your.domain.com
       DocumentRoot /var/www/yourwebsite

       ExpiresActive On

       # Set expiration headers for different file types
       ExpiresByType text/html "access plus 1 month"
       ExpiresByType text/css "access plus 1 year"
       ExpiresByType text/javascript "access plus 1 year"
       ExpiresByType image/jpeg "access plus 1 year"
       ExpiresByType image/png "access plus 1 year"

       # Additional VirtualHost configurations
   </VirtualHost>
   ```

   - Replace `your.domain.com` with your website's domain name.
   - Update `DocumentRoot` to the actual path of your website files.
   - Modify the `ExpiresByType` directives to define expiration headers for various file types.

4. **Save and Close the Configuration File.**

5. **Check Configuration Syntax:**

   Before applying the changes, it's essential to check the Apache configuration for any syntax errors. Use the following command:

   ```bash
   sudo apachectl configtest
   ```

6. **Reload Apache:**

   If the configuration test passes without errors, reload Apache to apply the changes:

   ```bash
   sudo systemctl reload apache2
   ```

7. **Testing:**

   Test the setup by accessing your website and checking the HTTP headers. Content should now be cached with the expiration headers you've set.

That's it! You've successfully configured Apache with `mod_expires` directives to control content caching. Customize the file types and expiration periods as needed for your specific caching requirements.

This README file provides a step-by-step guide to configure Apache with `mod_expires` directives for content caching control. Customize the file types and expiration periods as required for your specific use case.